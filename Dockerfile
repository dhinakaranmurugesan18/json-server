# Use an official Node.js runtime as the base image
FROM node:14

# Set a working directory
WORKDIR /app

# Install JSON Server globally
RUN npm install -g json-server

# Copy your JSON data file (e.g., db.json) into the container
COPY db.json /app

# Expose the JSON Server port (default is 3000)
EXPOSE 3000

# Start JSON Server when the container runs
CMD json-server --watch db.json --host 0.0.0.0 --port 3000
